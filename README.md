# Maps

## [Buildings]()

Building projects that I've worked on.

## [Summits](https://gitlab.com/MatthewSteen/map/-/blob/main/summits/summits.geojson)

Mountains that I've climbed or hiked.

<script src="https://gitlab.com/MatthewSteen/map/-/snippets/3703864.js"></script>

## [Wildfires]()

Wildland fires that I was assigned to during my career as a wildland firefighter from 2002-2010.
